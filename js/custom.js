$(document).ready(function ()
{

	$('.featured-products-tabs li a').click(function(){

		var id = $(this).attr('href').replace('#', '');
		
		$('.featured-products-tabs li').removeClass('active');
		$(this).parent().addClass('active');

		$('.products').hide();
		$('#' + id).addClass('products');
		$('#' + id).show();
		
		return false;
	});

	$('.featured-products-tabs li:first-child a').click();


	var owl1 = $('#owl_1');

	owl1.owlCarousel({
 
    autoPlay: 3000, //Set AutoPlay to 3 seconds
 
    items : 6,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,3]
      // navigation: true;
     
 
  	});
	$("next-view .next").click(function(){
   	 	owl1.trigger('owl1.next1');
  	})
 	 $("next-view .prev").click(function(){
   	 	owl1.trigger('owl1.prev1');
  	})	


 	var owl2 = $('#owl_2');

	owl2.owlCarousel({
 
    autoPlay: 8000, //Set AutoPlay to 3 seconds
 
    items : 6,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,3]
      // navigation: true;
     
 
  	});
	$("next-view .next").click(function(){
   	 	owl2.trigger('owl1.next2');
  	})
 	 $("next-view .prev").click(function(){
   	 	owl2.trigger('owl1.prev2');
  	})	


});